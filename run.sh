#!/bin/sh

QEMU="qemu-system-x86_64"
QEMUFLAGS="-no-reboot -debugcon stdio ${QEMUFLAGS_ext}"

set -e

ninja

mkdir -p iso/boot/grub
cat > iso/boot/grub/grub.cfg <<EOF
set timeout=0
menuentry "Kryx" {
    multiboot /boot/kryxk $@
}
EOF
cp kryxk iso/boot/kryxk

grub-mkrescue -o kryx.iso iso/

"${QEMU}" ${QEMUFLAGS} -cdrom kryx.iso
