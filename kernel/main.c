#include "cmdline.h"
#include "common.h"
#include "cpu.h"
#include "debug.h"
#include "descriptor.h"
#include "multiboot.h"
#include "pagealloc.h"
#include "paging.h"
#include "panic.h"
#include "string.h"

extern char _kstart, _end;

[[gnu::used]] noreturn void
kmain(uint32_t mb_magic, multiboot_info_t* mb_info) {
    init_cpu_caps();
    debug_printf("-- KRYX --\n");

    if (mb_magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        debug_printf("Bootloader is not Multiboot-compliant: %08X\n", mb_magic);
        panic();
    }

    if (!(mb_info->flags & MULTIBOOT_INFO_MEM_MAP)) {
        debug_printf("No memory map available\n");
        panic();
    }

    uintptr_t max_addr = 0, max_addr_p = 0;;
    debug_printf("Memory map:\n");
    for (size_t i = 0; i < mb_info->mmap_length;) {
        multiboot_mmap_entry_t* mmap = VPTR(mb_info->mmap_addr + i);
        i += mmap->size + 4;

        debug_printf(
            "* %d 0x%016lX (0x%016lX)\n", mmap->type, mmap->addr, mmap->len);

        if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE) {
            uintptr_t addr = MIN(mmap->addr, BITMAP0_MAX);
            uintptr_t limit = MIN(mmap->addr + mmap->len, BITMAP0_MAX);
            page_init_allow(addr, limit - addr);
            max_addr = MAX(max_addr, mmap->addr + mmap->len);
        }
        max_addr_p = MAX(max_addr_p, mmap->addr + mmap->len);
    }
    max_addr = ALIGN_UP(max_addr, PAGE_SIZE);

    page_init_reserve(0, 4096); // null
    page_init_reserve(0x80000, 0x20000); // EBDA

    page_init_reserve(KPADDR(&_kstart), &_end - &_kstart);
    page_init_reserve(KPADDR(mb_info), sizeof(multiboot_info_t));
    page_init_reserve(mb_info->mmap_addr, mb_info->mmap_length);

    if (mb_info->flags & MULTIBOOT_INFO_MODS) {
        page_init_reserve(
            mb_info->mods_addr,
            mb_info->mods_count * sizeof(multiboot_mod_entry_t));

        multiboot_mod_entry_t* mods = VPTR(mb_info->mods_addr);
        for (size_t i = 0; i < mb_info->mods_count; i++) {
            page_init_reserve(
                mods[i].mod_start, mods[i].mod_end - mods[i].mod_start);

            if (mods[i].cmdline)
                page_init_reserve(
                    mods[i].cmdline, strlen(VPTR(mods[i].cmdline)));
        }
    }

    if ((mb_info->flags & MULTIBOOT_INFO_CMDLINE) && mb_info->cmdline) {
        page_init_reserve(mb_info->flags, strlen(VPTR(mb_info->cmdline)));

        debug_printf("Command line: %s\n", (const char*)VPTR(mb_info->cmdline));
        handle_cmdline(VPTR(mb_info->cmdline));
    }

    if (max_addr > BITMAP0_MAX) {
        page_alloc_reinit(max_addr);

        for (size_t i = 0; i < mb_info->mmap_length;) {
            multiboot_mmap_entry_t* mmap = VPTR(mb_info->mmap_addr + i);
            i += mmap->size + 4;

            if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE) {
                uintptr_t addr = MAX(mmap->addr, BITMAP0_MAX);
                uintptr_t limit = MAX(mmap->addr + mmap->len, BITMAP0_MAX);
                page_init_allow(addr, limit - addr);
            }
        }
    }

    debug_printf("CPU capabilities:");
    for (size_t i = 0; i < CPU_CAP__MAX; i++) {
        if (CPU_CAPSI__X(i) & CPU_CAPSI__M(i))
            debug_printf(" %s", cpu_cap_names[i]);
    }
    debug_printf("\n");

    init_paging(max_addr_p);
    init_descriptors();

    debug_printf("%016lX\n", page_alloc(4096));
    debug_printf("%016lX\n", page_alloc(1024 * 1024));
    debug_printf("%016lX\n", page_alloc(4096));

    debug_printf("-- suya.. --\n");
    __builtin_trap();
    panic();
}
