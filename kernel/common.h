#pragma once

#define VBASE (0xFFFF800000000000UL)
#define KVBASE (0xFFFFFFFF80000000UL)
#define PAGE_SIZE (4096UL)

#ifndef ASM_FILE

#define va_list __builtin_va_list
#define va_start __builtin_va_start
#define va_end __builtin_va_end
#define va_copy __builtin_va_copy
#define va_arg __builtin_va_arg

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;

typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef signed long int64_t;

typedef uint64_t uintptr_t;
typedef int64_t intptr_t;

typedef uint64_t uintmax_t;
typedef int64_t intmax_t;

typedef uintptr_t size_t;
typedef intptr_t ptrdiff_t;
typedef intptr_t ssize_t;

#define noreturn _Noreturn
#define alignas _Alignas

#define PADDR(x) (((uintptr_t)x) - VBASE)
#define KPADDR(x) (((uintptr_t)x) - KVBASE)

#define VPTR(x) (void*)((x) + VBASE)

#define ABS(x) \
    ({ \
        typeof(x) __x = (x); \
        __x < 0 ? -__x : __x; \
    })

#define MAX(x, y) \
    ({ \
        typeof(x) __x = (x); \
        typeof(y) __y = (y); \
        __x > __y ? __x : __y; \
    })

#define MIN(x, y) \
    ({ \
        typeof(x) __x = (x); \
        typeof(y) __y = (y); \
        __x < __y ? __x : __y; \
    })

#define ALIGN_DOWN(x, y) \
    ({ \
        typeof(x) __x = (x); \
        typeof(y) __y = (y); \
        __x & ~(__y - 1); \
    })

#define ALIGN_UP(x, y) \
    ({ \
        typeof(x) __x = (x); \
        typeof(y) __y = (y); \
        (__x + __y - 1) & ~(__y - 1); \
    })

#define BSF(x) __builtin_ctzl(x)

#endif
