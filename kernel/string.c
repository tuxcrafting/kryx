#include "common.h"
#include "string.h"

void* memcpy(void* restrict dest, const void* restrict src, size_t n) {
    char* cdest = dest;
    const char* csrc = src;
    for (size_t i = 0; i < n; i++)
        cdest[i] = csrc[i];
    return dest;
}

void* memset(void* dest, int c, size_t n) {
    char* cdest = dest;
    for (size_t i = 0; i < n; i++)
        cdest[i] = c;
    return dest;
}

size_t strlen(const char* str) {
    size_t n = 0;
    for (size_t i = 0; str[i]; i++)
        n++;
    return n;
}

const char* strchr(const char* str, int c) {
    size_t i;
    for (i = 0; str[i]; i++)
        if (str[i] == c)
            return &str[i];
    return c ? 0 : &str[i];
}

char* strtok_r(
    char* restrict str, const char* restrict delim, char** restrict saveptr) {
    if (!*saveptr)
        *saveptr = str;

    if (!**saveptr)
        return 0;

    while (strchr(delim, **saveptr))
        ++*saveptr;

    if (!**saveptr)
        return 0;

    char* r = *saveptr;

    while (**saveptr && !strchr(delim, **saveptr))
        ++*saveptr;

    if (**saveptr) {
        **saveptr = 0;
        ++*saveptr;
    }

    return r;
}

int strcmp(const char* a, const char* b) {
    for (size_t i = 0;; i++) {
        int t = a[i] - b[i];
        if (t)
            return t;
        if (a[i] == 0)
            return 0;
    }
}
