#include "assert.h"
#include "common.h"
#include "cpu.h"
#include "pagealloc.h"
#include "paging.h"
#include "string.h"
#include "x86.h"

void init_paging(uintptr_t max_addr) {
    pml4_base[0] = 0;
    for (size_t i = 0; i < 2048; i++)
        pml2_base[i] |= PTE_G;

    max_addr = ALIGN_UP(max_addr, 0x40000000UL);

    for (uintptr_t addr = 0x100000000UL; addr < max_addr; addr += 0x40000000UL) {
        size_t pml4i = (addr >> 39 & 255) + 256;
        size_t pml3i = addr >> 30 & 511;

        uint64_t pml4e = pml4_base[pml4i];
        if (!(pml4e & PTE_P)) {
            uintptr_t t = page_alloc(PAGE_SIZE);
            memset(VPTR(t), 0, PAGE_SIZE);
            pml4e = pml4_base[pml4i] = t | PTE_P | PTE_W;
        }
        uint64_t* pml3 = VPTR(pml4e & PTE_MASK);

        assert(!(pml3[pml3i] & PTE_P));
        pml3[pml3i] = addr | PTE_P | PTE_W | PTE_SZ | PTE_G;
    }

    W_CR(3, KPADDR(pml4_base));
}
