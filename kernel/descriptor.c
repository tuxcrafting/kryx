#include "common.h"
#include "descriptor.h"
#include "x86.h"

[[gnu::used]] uint64_t gdt[GDT_WORDS] = {
    // SEG_NULL
    0x0000000000000000UL,
    0x0000000000000000UL,
    // SEG_CS0
    0x00AF98000000FFFFUL,
    // SEG_DS0
    0x00CF92000000FFFFUL,
    // SEG_CS3
    0x00AFF8000000FFFFUL,
    // SEG_DS3
    0x00CFF2000000FFFFUL,
    // SEG_TSS
    0x0000890000000000UL,
    0x0000000000000000UL,
};
uint64_t idt[IDT_WORDS];
tss_t tss;

#define IST_WORDS (8192)

static uint64_t ist1[IST_WORDS];

static void
set_idt_entry(size_t i, uintptr_t offset, int dpl, int type, int ist) {
    idt[i * 2] = 0;
    idt[i * 2] |= (offset >> 0 & 0xFFFFUL) << 0;
    idt[i * 2] |= (offset >> 16 & 0xFFFFUL) << 48;
    idt[i * 2] |= ((uint64_t)ist) << 32;
    idt[i * 2] |= ((uint64_t)type) << 40;
    idt[i * 2] |= ((uint64_t)dpl) << 45;
    idt[i * 2] |= 1UL << 47;
    idt[i * 2] |= SEG_CS0 << 16;
    idt[i * 2 + 1] = offset >> 32;
}

#define SET_INT(n, dpl) \
    extern char i_##n; \
    set_idt_entry(n, (uintptr_t)&i_##n, dpl, IDTE_INT, 1)

void init_descriptors() {
    gdt[6] |= (((uintptr_t)&tss) >> 0 & 0xFFFFFFUL) << 16;
    gdt[6] |= (((uintptr_t)&tss) >> 24 & 0xFFUL) << 56;
    gdt[6] |= sizeof(tss) - 1;
    gdt[7] |= ((uintptr_t)&tss) >> 32;

    tss.ist_rsp[1] = (uintptr_t)&ist1[IST_WORDS];

    SET_INT(0x00, 0);
    SET_INT(0x01, 0);
    SET_INT(0x02, 0);
    SET_INT(0x03, 0);
    SET_INT(0x04, 0);
    SET_INT(0x05, 0);
    SET_INT(0x06, 0);
    SET_INT(0x07, 0);
    SET_INT(0x08, 0);
    SET_INT(0x0A, 0);
    SET_INT(0x0B, 0);
    SET_INT(0x0C, 0);
    SET_INT(0x0D, 0);
    SET_INT(0x0E, 0);
    SET_INT(0x10, 0);
    SET_INT(0x11, 0);
    SET_INT(0x12, 0);
    SET_INT(0x13, 0);
    SET_INT(0x14, 0);
    SET_INT(0x15, 0);
    SET_INT(0x1D, 0);
    SET_INT(0x1E, 0);

    dtr_t idtr = {.limit = sizeof(idt) - 1, .base = (uintptr_t)idt};
    LIDT(idtr);

    LLDT(SEG_NULL);
    LTR(SEG_TSS);
}
