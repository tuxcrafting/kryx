#pragma once

#include "common.h"

enum {
    CPU_CAP_FPU,
    CPU_CAP_APIC,
    CPU_CAP_MMX,
    CPU_CAP_FXSR,
    CPU_CAP_SSE,
    CPU_CAP_SSE2,
    CPU_CAP_SSE3,
    CPU_CAP_PCLMULQDQ,
    CPU_CAP_SSSE3,
    CPU_CAP_SSE4_1,
    CPU_CAP_SSE4_2,
    CPU_CAP_AESNI,
    CPU_CAP_XSAVE,
    CPU_CAP__MAX,
};

#define CPU_CAPS_N ((CPU_CAP__MAX + 31) / 32)
extern uint32_t cpu_caps[CPU_CAPS_N];
extern const char* cpu_cap_names[CPU_CAP__MAX];

#define CPU_CAPS__X(t) cpu_caps[CPU_CAP_##t / 32]
#define CPU_CAPS__M(t) (1 << CPU_CAP_##t % 32)
#define HAS_CPU_CAP(t) (CPU_CAPS__X(t) & CPU_CAPS__M(t))
#define SET_CPU_CAP(t) (CPU_CAPS__X(t) |= CPU_CAPS__M(t))
#define CLEAR_CPU_CAP(t) (CPU_CAPS__X(t) &= ~CPU_CAPS__M(t))

#define CPU_CAPSI__X(i) cpu_caps[i / 32]
#define CPU_CAPSI__M(i) (1 << i % 32)

#define SET_CPU_CAP_IF(cond, t) \
    if (cond) \
    SET_CPU_CAP(t)

void init_cpu_caps();
