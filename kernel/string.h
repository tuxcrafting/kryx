#pragma once

#include "common.h"

void* memcpy(void* restrict dest, const void* restrict src, size_t n);
void* memset(void* dest, int c, size_t n);
size_t strlen(const char* str);
const char* strchr(const char* str, int c);
char* strtok_r(
    char* restrict str, const char* restrict delim, char** restrict saveptr);
int strcmp(const char* a, const char* b);
