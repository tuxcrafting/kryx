#pragma once

#include "common.h"

#define BITMAP0_MAX (0x100000000UL)

uintptr_t page_alloc(size_t length);
void page_free(uintptr_t addr, size_t length);
void page_init_reserve(uintptr_t base, size_t length);
void page_init_allow(uintptr_t base, size_t length);
void page_alloc_reinit(uintptr_t max_addr);
