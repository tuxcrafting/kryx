#include "common.h"
#include "debug.h"
#include "interrupts.h"
#include "panic.h"

#define PRINT_REG_(reg, f) debug_printf(#reg " = 0x" f "\n", st->reg)
#define PRINT_REG(reg) PRINT_REG_(reg, "%016lX")
#define PRINT_SREG(reg) PRINT_REG_(reg, "%04X")

#define WRAPPER_(n, e) \
    __asm__(".global i_" #n \
            ";" \
            "i_" #n ":" e \
            "pushq %rax;" \
            "pushq %rcx;" \
            "pushq %rdx;" \
            "pushq %rbx;" \
            "pushq %rsp;" \
            "pushq %rbp;" \
            "pushq %rsi;" \
            "pushq %rdi;" \
            "pushq %r8;" \
            "pushq %r9;" \
            "pushq %r10;" \
            "pushq %r11;" \
            "pushq %r12;" \
            "pushq %r13;" \
            "pushq %r14;" \
            "pushq %r15;" \
            "mov %gs, %ax;" \
            "pushq %rax;" \
            "mov %fs, %ax;" \
            "pushq %rax;" \
            "mov %ds, %ax;" \
            "pushq %rax;" \
            "mov %es, %ax;" \
            "pushq %rax;" \
            "mov %rsp, %rdi;" \
            "mov $" #n \
            ", %rsi;" \
            "mov $0x0018, %rax;" \
            "mov %ax, %es;" \
            "mov %ax, %ds;" \
            "mov %ax, %fs;" \
            "mov %ax, %gs;" \
            "call int_handler;")

#define WRAPPER_NOERRC(n) WRAPPER_(n, "pushq $0;")
#define WRAPPER_ERRC(n) WRAPPER_(n, "")

WRAPPER_NOERRC(0x00);
WRAPPER_NOERRC(0x01);
WRAPPER_NOERRC(0x02);
WRAPPER_NOERRC(0x03);
WRAPPER_NOERRC(0x04);
WRAPPER_NOERRC(0x05);
WRAPPER_NOERRC(0x06);
WRAPPER_NOERRC(0x07);
WRAPPER_NOERRC(0x08);
WRAPPER_NOERRC(0x09);
WRAPPER_ERRC(0x0A);
WRAPPER_ERRC(0x0B);
WRAPPER_ERRC(0x0C);
WRAPPER_ERRC(0x0D);
WRAPPER_NOERRC(0x0E);
WRAPPER_NOERRC(0x10);
WRAPPER_ERRC(0x11);
WRAPPER_NOERRC(0x12);
WRAPPER_NOERRC(0x13);
WRAPPER_NOERRC(0x14);
WRAPPER_ERRC(0x15);
WRAPPER_ERRC(0x1D);
WRAPPER_ERRC(0x1E);

[[gnu::used]] noreturn void int_handler(int_state_t* st, uint8_t type) {
    debug_printf("interrupt #0x%02X (e = 0x%04X)\n", type, st->errc);
    PRINT_SREG(es);
    PRINT_SREG(cs);
    PRINT_SREG(ss);
    PRINT_SREG(ds);
    PRINT_SREG(fs);
    PRINT_SREG(gs);
    PRINT_REG(rflags);
    PRINT_REG(rip);
    PRINT_REG(rax);
    PRINT_REG(rcx);
    PRINT_REG(rdx);
    PRINT_REG(rbx);
    PRINT_REG(rsp);
    PRINT_REG(rbp);
    PRINT_REG(rsi);
    PRINT_REG(rdi);
    PRINT_REG(r8);
    PRINT_REG(r9);
    PRINT_REG(r10);
    PRINT_REG(r11);
    PRINT_REG(r12);
    PRINT_REG(r13);
    PRINT_REG(r14);
    PRINT_REG(r15);
    panic();
}
