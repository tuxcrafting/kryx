#include "assert.h"
#include "common.h"
#include "pagealloc.h"
#include "string.h"

#define BITMAP0_WORDS (BITMAP0_MAX / PAGE_SIZE / 64)
alignas(PAGE_SIZE) static uint64_t bitmap0[BITMAP0_WORDS];

static size_t last_allocd_i = 0;
static size_t bitmap_words = BITMAP0_WORDS;
static uint64_t* bitmap = VPTR(KPADDR(bitmap0));

static void mark(uintptr_t addr, int check, int val) {
    assert(addr < bitmap_words * PAGE_SIZE * 64);
    size_t i = addr / PAGE_SIZE / 64;
    size_t j = addr / PAGE_SIZE % 64;
    int t = bitmap[i] >> j & 1;
    if (check)
        assert(t != val);

    if (val)
        bitmap[i] |= 1UL << j;
    else
        bitmap[i] &= ~(1UL << j);
}

static void mark_range(uintptr_t base, uintptr_t limit, int check, int val) {
    while (base < limit) {
        mark(base, check, val);
        base += PAGE_SIZE;
    }
}

uintptr_t page_alloc(size_t length) {
    length = ALIGN_UP(length, PAGE_SIZE);
    if (!length)
        return 0;
    for (size_t it = 0; it < bitmap_words; it++) {
        size_t i = it + last_allocd_i;
        if (i >= bitmap_words)
            i -= bitmap_words;

        if (!bitmap[i])
            continue;

        size_t j = BSF(bitmap[i]);
        uintptr_t addr = (i * 64 + j) * PAGE_SIZE;

        int ok = 1;
        for (size_t off = 4096; off < length; off += PAGE_SIZE) {
            size_t i1 = (addr + off) / PAGE_SIZE / 64;
            size_t j1 = (addr + off) / PAGE_SIZE % 64;

            if (!(bitmap[i1] & 1UL << j1)) {
                ok = 0;
                break;
            }
        }

        if (ok) {
            last_allocd_i = i;
            mark_range(addr, addr + length, 0, 0);
            return addr;
        }
    }

    return 0;
}

void page_free(uintptr_t addr, size_t length) {
    if (!addr || !length)
        return;
    assert(!(addr % PAGE_SIZE));
    length = ALIGN_UP(length, PAGE_SIZE);
    mark_range(addr, addr + length, 1, 1);
}

void page_init_reserve(uintptr_t base, size_t length) {
    uintptr_t limit = ALIGN_UP(base + length, PAGE_SIZE);
    base = ALIGN_DOWN(base, PAGE_SIZE);
    mark_range(base, limit, 0, 0);
}

void page_init_allow(uintptr_t base, size_t length) {
    uintptr_t limit = ALIGN_DOWN(base + length, PAGE_SIZE);
    base = ALIGN_UP(base, PAGE_SIZE);
    mark_range(base, limit, 0, 1);
}

void page_alloc_reinit(uintptr_t max_addr) {
    max_addr = ALIGN_UP(max_addr, PAGE_SIZE * 8 * PAGE_SIZE);
    size_t new_words = max_addr / PAGE_SIZE / 64;
    uint64_t* new_bitmap = VPTR(page_alloc(new_words * 8));
    memcpy(new_bitmap, bitmap, bitmap_words * 8);
    memset(&new_bitmap[bitmap_words], 0, (new_words - bitmap_words) * 8);
    size_t old_words = bitmap_words;
    uint64_t* old_bitmap = bitmap;
    bitmap_words = new_words;
    bitmap = new_bitmap;
    page_free(PADDR(old_bitmap), old_words * 8);
}
