#pragma once

#include "common.h"
#include "x86.h"

#define SEG_NULL (0x0000)
#define SEG_CS0 (0x0010)
#define SEG_DS0 (0x0018)
#define SEG_CS3 (0x0023)
#define SEG_DS3 (0x002B)
#define SEG_TSS (0x0030)

#define GDT_WORDS (8)
extern uint64_t gdt[GDT_WORDS];

#define IDT_WORDS (512)
extern uint64_t idt[IDT_WORDS];

extern tss_t tss;

void init_descriptors();
