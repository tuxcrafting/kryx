#include "cmdline.h"
#include "cpu.h"
#include "debug.h"
#include "string.h"

void handle_cmdline(char* cmdline) {
    char* saveptr = 0;
    const char* tok;
    while ((tok = strtok_r(cmdline, " ", &saveptr))) {
        if (tok[0] == '+' || tok[0] == '-') {
            for (size_t i = 0; i < CPU_CAP__MAX; i++) {
                if (cpu_cap_names[i]
                    && strcmp(&tok[1], cpu_cap_names[i]) == 0) {
                    if (tok[0] == '+')
                        CPU_CAPSI__X(i) |= CPU_CAPSI__M(i);
                    else
                        CPU_CAPSI__X(i) &= ~CPU_CAPSI__M(i);
                    break;
                }
            }
        }
    }
}
