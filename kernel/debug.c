#include "assert.h"
#include "common.h"
#include "debug.h"
#include "nanoprintf.h"
#include "x86.h"

#define DEBUG_PORT 0xE9

noreturn void __assert_fail(const char* msg, const char* file, int line) {
    debug_printf("assertion failed: %s:%d: %s\n", file, line, msg);
    __builtin_trap();
}

#ifndef NDEBUG

static char debug_mode;

void debug_putc(int c, void* ctx) {
    if (!debug_mode)
        debug_mode = (IN(DEBUG_PORT) == DEBUG_PORT) + 1;

    if (debug_mode == 2)
        OUT(DEBUG_PORT, c);
}

int debug_printf(const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    int r = npf_vpprintf(debug_putc, 0, fmt, ap);
    va_end(ap);
    return r;
}

int debug_vprintf(const char* fmt, va_list ap) {
    return npf_vpprintf(debug_putc, 0, fmt, ap);
}

#endif
