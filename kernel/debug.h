#pragma once

#include "common.h"

#ifndef NDEBUG

void debug_putc(int c, void* ctx);
[[gnu::format(printf, 1, 2)]] int debug_printf(const char* fmt, ...);
[[gnu::format(printf, 1, 0)]] int debug_vprintf(const char* fmt, va_list ap);

#else

#define debug_putc(c, ctx)
#define debug_printf(fmt, ...)
#define debug_vprintf(fmt, ap)

#endif
