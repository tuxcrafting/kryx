#include "common.h"
#include "cpu.h"
#include "x86.h"

uint32_t cpu_caps[CPU_CAPS_N];
const char* cpu_cap_names[CPU_CAP__MAX] = {
    [CPU_CAP_FPU] = "fpu",
    [CPU_CAP_APIC] = "apic",
    [CPU_CAP_MMX] = "mmx",
    [CPU_CAP_FXSR] = "fxsr",
    [CPU_CAP_SSE] = "sse",
    [CPU_CAP_SSE2] = "sse2",
    [CPU_CAP_SSE3] = "sse3",
    [CPU_CAP_PCLMULQDQ] = "pclmulqdq",
    [CPU_CAP_SSSE3] = "ssse3",
    [CPU_CAP_SSE4_1] = "sse4_1",
    [CPU_CAP_SSE4_2] = "sse4_2",
    [CPU_CAP_AESNI] = "aesni",
    [CPU_CAP_XSAVE] = "xsave",
};

void init_cpu_caps() {
    uint32_t cpuid_01_c, cpuid_01_d;
    __asm__ volatile("cpuid"
                     : "=c"(cpuid_01_c), "=d"(cpuid_01_d)
                     : "a"(0x00000001U)
                     : "%ebx");

    SET_CPU_CAP_IF(cpuid_01_d & CPUID_01_D_FPU, FPU);
    SET_CPU_CAP_IF(cpuid_01_d & CPUID_01_D_APIC, APIC);
    SET_CPU_CAP_IF(cpuid_01_d & CPUID_01_D_MMX, MMX);
    SET_CPU_CAP_IF(cpuid_01_d & CPUID_01_D_FXSR, FXSR);
    SET_CPU_CAP_IF(cpuid_01_d & CPUID_01_D_SSE, SSE);
    SET_CPU_CAP_IF(cpuid_01_d & CPUID_01_D_SSE2, SSE2);

    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_SSE3, SSE3);
    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_PCLMULQDQ, PCLMULQDQ);
    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_SSSE3, SSSE3);
    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_SSE4_1, SSE4_1);
    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_SSE4_2, SSE4_2);
    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_AESNI, AESNI);
    SET_CPU_CAP_IF(cpuid_01_c & CPUID_01_C_XSAVE, XSAVE);
}
