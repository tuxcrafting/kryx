#pragma once

#include "common.h"

typedef struct int_state int_state_t;
struct int_state {
    uint16_t es, _es[3];
    uint16_t ds, _ds[3];
    uint16_t fs, _fs[3];
    uint16_t gs, _gs[3];
    uint64_t r15, r14, r13, r12;
    uint64_t r11, r10, r9, r8;
    uint64_t rdi, rsi, rbp, _rsp;
    uint64_t rbx, rdx, rcx, rax;
    uint16_t errc, _errc[3];
    uint64_t rip;
    uint16_t cs, _cs[3];
    uint64_t rflags;
    uint64_t rsp;
    uint16_t ss, _ss[3];
};

noreturn void int_handler(int_state_t* st, uint8_t type);
