#pragma once

#define PTE_P (1UL << 0)
#define PTE_W (1UL << 1)
#define PTE_U (1UL << 2)
#define PTE_PWT (1UL << 3)
#define PTE_PCD (1UL << 4)
#define PTE_A (1UL << 5)
#define PTE_D (1UL << 6)
#define PTE_SZ (1UL << 7)
#define PTE_G (1UL << 8)
#define PTE_MASK (0xFFFFFFFFFFFFF000UL)

#define FLAGS_ID (1UL << 21)

#define CR0_PG (1UL << 31)

#define CR4_PAE (1UL << 5)
#define CR4_OSFXSR (1UL << 9)
#define CR4_OSXMEX (1UL << 10)

#define MSR_EFER (0xC0000080U)

#define EFER_LME (1UL << 8)

#define CPUID_01_D_FPU (1U << 0)
#define CPUID_01_D_APIC (1U << 9)
#define CPUID_01_D_MMX (1U << 23)
#define CPUID_01_D_FXSR (1U << 24)
#define CPUID_01_D_SSE (1U << 25)
#define CPUID_01_D_SSE2 (1U << 26)
#define CPUID_01_C_SSE3 (1U << 0)
#define CPUID_01_C_PCLMULQDQ (1U << 1)
#define CPUID_01_C_SSSE3 (1U << 9)
#define CPUID_01_C_SSE4_1 (1U << 19)
#define CPUID_01_C_SSE4_2 (1U << 20)
#define CPUID_01_C_AESNI (1U << 25)
#define CPUID_01_C_XSAVE (1U << 26)

#define IDTE_INT (0b1110)
#define IDTE_TRAP (0b1111)

#ifndef ASM_FILE

#include "common.h"

#define IN(port) \
    ({ \
        uint8_t __x; \
        __asm__ volatile("in %%dx, %%al" : "=a"(__x) : "d"(port)); \
        __x; \
    })

#define OUT(port, x) __asm__ volatile("out %%al, %%dx" : : "d"(port), "a"(x))

#define R_CR(n) \
    ({ \
        uint64_t __x; \
        __asm__ volatile("mov %%cr" #n ", %0" : "=r"(__x)); \
        __x; \
    })
#define W_CR(n, x) __asm__ volatile("mov %0, %%cr" #n : : "r"((uint64_t)(x)));
#define SC_CR(n, s, c) W_CR(n, (R_CR(n) | (s)) & ~(c))

#define LGDT(dtr) __asm__ volatile("lgdt %0" : : "m"(dtr))
#define LIDT(dtr) __asm__ volatile("lidt %0" : : "m"(dtr))
#define LLDT(seg) __asm__ volatile("lldt %0" : : "a"((uint16_t)(seg)))
#define LTR(seg) __asm__ volatile("ltr %0" : : "a"((uint16_t)(seg)))

typedef struct dtr dtr_t;
struct [[gnu::packed]] dtr {
    uint16_t limit;
    uint64_t base;
};

typedef struct tss tss_t;
struct [[gnu::packed]] tss {
    uint32_t _0;
    uint64_t rsp[3];
    uint64_t ist_rsp[8];
    uint32_t _1, _2, _3;
};

#endif
