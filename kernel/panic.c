#include "common.h"
#include "panic.h"

noreturn void panic() {
    __asm__ volatile("1: cli; hlt; jmp 1b");
    __builtin_trap();
}
