#pragma once

#include "common.h"

noreturn void __assert_fail(const char* msg, const char* file, int line);

#ifdef NDEBUG
#define assert(cond)
#else
#define assert(cond) \
    ((cond) ? (void)0 : __assert_fail(#cond, __FILE__, __LINE__))
#endif
