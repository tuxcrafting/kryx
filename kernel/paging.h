#pragma once

#include "common.h"

extern uint64_t pml4_base[512];
extern uint64_t pml3_base[512 * 2];
extern uint64_t pml2_base[512 * 4];

void init_paging(uintptr_t max_addr);
